// Config
global_ttrssUrl = "/ttrss";

// Preferences
pref_Feed = '-4'; // Default: all items
pref_ViewMode = 'unread'; // Default: unread articles only
// Not used -- pref_TextType = 'content'; // Default: full articles
pref_OrderBy = 'feed_dates'; // Default: oldest first
pref_FeedSort = '0'; // Default: Do not sort feeds
pref_StartInCat = '1'; // Default: Start showing feeds
//added so user can change feed limit easily. 
pref_Feed_limit = 5; // Default: Load 25 Items...
