# README #

## Instructions are Preliminary, and may not work ##

### What is this repository for? ###

* A Openshift v3 Repository for the learning tier.
### It has the following: ###

1.	[tt-rss](https://tt-rss.org/),
1.	[owncloud](https://owncloud.org/),
1.	[RSS-Bridge](https://github.com/RSS-Bridge/rss-bridge), for those sites without feeds
1.	[g2ttrss-mobile](https://github.com/g2ttrss/g2ttrss-mobile), for mobile access to tt-rss.
1.	This openshift project uses php and mariadb.

* All the applications on this project are manually updated to the git repository.

### It is recommended that you fork this repository to cover your own needs ###
* When the repository is updated, you may need to update the tt-rss or owncloud database for your applications to continue to work.

### How do I get set up? ###
	

1.	Get an [openshift](https://www.openshift.com/learn/get-started/) account.
1.	Create a [new project](https://docs.openshift.com/online/getting_started/basic_walkthrough.html#bw-creating-a-new-application).
1.	Download the [oc client tools](https://www.openshift.org/download.html) and follow the [instructions](https://docs.openshift.com/enterprise/3.0/cli_reference/get_started_cli.html) to set it up (the path, howto login, etc.)
1.	Create the project, replacing the various <passwords> with your own:

	>oc new-app php~https://bitbucket.org/szing/base.git 
	>mariadb 
	>--group=php+mariadb 
	>-e MYSQL_USER=db 
	>-e MYSQL_PASSWORD=<db password> 
	>-e MYSQL_DATABASE=db 
	>-e MYSQL_ROOT_PASSWORD=<root password> 
	>--name=base

1.	On the Web Interface do the following (Change strategy to Recreate):
	1.	Application -> Deployments
	1.	Base
	1.	Actions -> Edit (Upper right hand side)
	1.	Change the Strategy Type to: Recreate.
	1.	On the bottom of the page, check "Pause Rollouts this deployment config"
	1.	Save
1.	On the web interface, do the following (Create Persistant Mount):
	1.	Storage.
	1.	Create Storage
1.	On the web interface, do the following (Apply persistant mount):
	1.	Application -> Pods
	1.	Choose base pod that is running
	1.	Actions -> Add storage (upper left hand side)
	1.	On mount path type in:

	>/var/lib/mysql/data

	1.	Add
1.	On the web interface, do the following (Unpause Deployment):
	1.	Choose unpause deployment, and wait for the deployment to start.
	1.	To force the build to start immediately, of the right and side choose the 3 vertical dots and choose start build.
	1.	Wait until the build is complete, and the pods have started.

####Setting up tt-rss and owncloud####
1.	Creating the persistant directories required by tt-rss and owncloud:

	Get the pod name.  The pod name is after the pods/:
	
	>oc get pods -a=false -o=name
	
	rsh into the pod and create the required directories:
	
	>oc rsh <pod name>
	>
	>cd /var/lib/mysql/data
	>
	>mkdir -p index ttrss/cache ttrss/config ttrss/feed-icons ttrss/lock ttrss/plugins.local ttrss/themes.local oc/apps oc/config backup
	
	remove the soft linked config file so you can install tt-rss
	
	>cd
	>
	>rm ttrss/config.php

####Setup tt-rss####
	1.	On the web interface, do the following:
		1.	Overview.
		2.	Click on the > sign to expand your deployment config.
		3.	Click on the link under Routes- External Traffice.
		4.	A new web page should open up, add the the address it opens up into tt-rss
		5.	Setup tt-rss.
			1.	The database is db
			2.	The database password is the password you created up above in <db password>
			3.	The database hostname is base.<project name>.svc
			4.	Complete the configuration.
	2.	On the rsh command line, do the following:

		cd
		mv /ttrss/config.php /var/lib/mysql/data/ttrss/config
		ln -sf ttrss/config.php /var/lib/mysql/data/ttrss/config.php
		exit

	3.	Sign into tt-rss with (username: admin, password: password)
####Adding your own plugins, themes, and email capability:####
	1.	Copy the files from your persistant directory:

		#Get the pod name.  The pod name is after the pods/:
		oc get pods -a=false -o=name
		oc rsync -c base <pod name>:/var/lib/mysql/data/ttrss <save directory>

	2.	To add plugins and themes, save them in the appropirate ttrss/plugins.local or ttrss/themes.local directory.
	3.	To edit the configuration file edit the file ttrss/config/config.php
		1.	To allow for tt-rss to send email edit the section: Email and digest settings
		2.	For example, to allow tt-rss to send emails from your gmail account set the following:
			1.	define('SMTP_SERVER', 'smtp.gmail.com:465');
			2.	define('SMTP_LOGIN', '<gmail@address');
			3.	define('SMTP_PA7SSWORD', '<gmail password');
			4.	define('SMTP_SECURE', 'ssl');
	4.	Upload your changes to openshift:
		oc rsync -c base <save directory> <pod name>:/var/lib/mysql/data

####Setting up cron for tt-rss####
	1.	Create an account at [cron-job.org](https://cron-job.org).
	2.	Create an Cronjob:
		1.	Cronjob
		2.	Create Cronjob
		3.	Title: ttrss
		4.	url: <openshift link>/ttrss/public.php?op=globalUpdateFeeds&daemon=1
		5.	User Defined Execution Times:
			1.	Every Day of the month
			2.	Every Day of the week
			3.	Every Month
			4.	Hours 6-22
			5.	At minutes 0, 15, 30, 45 (or what you want)
			6.	Notify when: the cronjob will be disabled because of too many failures
			7.	Request Method: Get
			8.	Enable Cron Job.
			9.	Save
####Creating your own home page:####
	1.	Create an index directory.
	2.	Inside the index directory, name your home page index.html
	3.	Upload the file to openshift
		#Get the pod name.  The pod name is after the pods/:
		oc get pods -a=false -o=name
		oc rsync -c base <save directory> <pod name>:/var/lib/mysql/data

####Using g2ttrss-mobile:####
	1.  Enable API Access:
	2.	On your tt-rss website:
		1.	Actions -> Preferences
		2.	Check Mark Enable API Access and click "Save Configuration"
		3.	The website will be on your <openshift link>/m
####Using RSS-Bridge:####
	1.	RSS-Bridge is only available on the localhost.  To access it, do the following:
		oc get pods -a=false -o=name
		oc rsh <pod name> 8000:8080
	2.	On your web browser go to
		localhost:8000/rssbase
	3.	You subscribe to the feed at:
		http://localhost:8080/rssbase/<rss-bridge string>

####Using owncloud:	Not yet documented.	####

####Backup Database:####

	#get the pod name
	oc get pods -a=false -o=name
	#backup the databases
	oc exec <pod name> -c base-1 -- /usr/bin/sh -c 'PATH=$PATH:/opt/rh/rh-mariadb101/root/usr/bin:/opt/app-root/src/bin:/opt/app-root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin mysqldump -h base.<project name>.svc -u $MYSQL_USER --password=$MYSQL_PASSWORD --all-databases | gzip > /var/lib/mysql/data/backup/db-sql-$(date +"%Y%m%d").gz'
	#copy the backup to the local computer
	oc rsync -c base-1 <pod name>:/var/lib/mysql/data/backup <backup directory>
	#remove the backup copy:
	oc exec $pod -c base-1 rm /var/lib/mysql/data/backup/db-sql-$(date +"%Y%m%d").gz
	

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
